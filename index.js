/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Splash from './src/screens/authentication/Splash';
import Slider from './src/screens/authentication/Slider';
import Signup from './src/screens/authentication/Signup';
import Home from './src/screens/dashboard/Home';
import Hotels from './src/screens/hotels/Hotels';
import Login from './src/screens/authentication/Login';
import Description from './src/screens/description/Description';
import Favorlist from './src/screens/favorList/Favorlist';
import Privacy from './src/screens/privacy/Privacy';
import TermsAndConditions from './src/screens/termsAndConditions/TermsAndConditions';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
AppRegistry.registerComponent(appName, () => App);
