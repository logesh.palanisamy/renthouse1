import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  RefreshControl,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import {FONTS, COLORS} from '../../constants/Theme';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import MAIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import IIcon from 'react-native-vector-icons/Ionicons';
import EIcon from 'react-native-vector-icons/Entypo';
import FIcon from 'react-native-vector-icons/FontAwesome5';
import {ScaledSheet} from 'react-native-size-matters';
import {ScrollView} from 'react-native-gesture-handler';
import database from '@react-native-firebase/database';

// pulltorefresh

const wait = timeout => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};

const Home = ({navigation}) => {
  const DATA = [
    {
      name: '1',
      FIcon: true,
      subtitle: 'Hotel',
    },
    {
      name: '2',
      MIcon: true,
      subtitle: 'House',
    },
    {
      name: '3',
      FAIcon: true,
      subtitle: 'Resort',
    },
    {
      name: '4',
      MAIcon: true,
      subtitle: 'Flat',
    },
    {
      name: '5',
      FIcon: true,
      subtitle: 'Hotel',
    },
  ];

  // const Homescroll1 = [
  //   {
  //     name: '1',
  //     image:
  //       'https://images.unsplash.com/photo-1566073771259-6a8506099945?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  //     title: 'NC Resorts',
  //     price: '$2200/month',
  //     placetext: '1691 Old Bayshore Hwy',
  //     bedtext: '3 Beds',
  //     bathtext: '4 Baths',
  //     sqft: '2450 sqft',
  //   },
  //   {
  //     name: '2',
  //     image:
  //       'https://images.unsplash.com/photo-1596436889106-be35e843f974?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  //     title: 'King House',
  //     price: '$3200/month',
  //     placetext: '1691 Old Bayshore Hwy',
  //     bedtext: '2 Beds',
  //     bathtext: '3 Baths',
  //     sqft: '3450 sqft',
  //   },
  //   {
  //     name: '3',
  //     image:
  //       'https://images.unsplash.com/photo-1596178067639-5c6e68aea6dc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  //     title: 'Rage Resorts',
  //     price: '$2060/month',
  //     placetext: '1691 Old Bayshore Hwy',
  //     bedtext: '4 Beds',
  //     bathtext: '3 Baths',
  //     sqft: '4450 sqft',
  //   },
  //   {
  //     name: '4',
  //     image:
  //       'https://images.unsplash.com/photo-1617859047452-8510bcf207fd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
  //     title: '0MG Resorts',
  //     price: '$3200/month',
  //     placetext: '1691 Old Bayshore Hwy',
  //     bedtext: '3 Beds',
  //     bathtext: '4 Baths',
  //     sqft: '2450 sqft',
  //   },
  //   {
  //     name: '5',
  //     image:
  //       'https://images.unsplash.com/photo-1564501049412-61c2a3083791?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1332&q=80',
  //     title: 'God Gifts Resorts',
  //     price: '$1200/month',
  //     placetext: '1691 Old Bayshore Hwy',
  //     bedtext: '3 Beds',
  //     bathtext: '4 Baths',
  //     sqft: '2450 sqft',
  //   },
  // ];

  const toprated = [
    {
      name: '1',
      image:
        'https://images.unsplash.com/photo-1564501049412-61c2a3083791?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1332&q=80',
      title: 'God Resorts',
      price: '$1200/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '3 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },
    {
      name: '2',
      image:
        'https://images.unsplash.com/photo-1617859047452-8510bcf207fd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: '0MG Resorts',
      price: '$3200/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '3 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },
    {
      name: '3',
      image:
        'https://images.unsplash.com/photo-1596178067639-5c6e68aea6dc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'Rage Resorts',
      price: '$2060/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '4 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },
    {
      name: '4',
      image:
        'https://images.unsplash.com/photo-1596436889106-be35e843f974?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'King House',
      price: '$1200/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '2 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },
    {
      name: '5',
      image:
        'https://images.unsplash.com/photo-1566073771259-6a8506099945?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'NC Resorts',
      price: '$2200/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '3 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },
  ];

  // loader
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 700);
  }, []);

  const [loading, setLoading] = useState(true);

  const [resorts, setResorts] = useState([]);

  // pull to refresh

  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    wait(2000).then(() => setRefreshing(false));
  }, []);

  // database
  useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      const Hotels = Object.values(data);
      setResorts(Hotels);
      setLoading(false);
    });
  }, []);
  let itemsRef = database().ref(`/Items`);

  return (
    <SafeAreaView style={styles.container}>
      {/* pulltorefresh */}

      <ScrollView
        contentContainerStyle={styles.scrollView}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        {/* header */}
        <View style={styles.headerView}>
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <EIcon name="menu" size={34} color="red" />
          </TouchableOpacity>

          <View style={{alignSelf: 'center'}}>
            <Text style={styles.headerTxt}>Rent House</Text>
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('FavStackScreen')}
            style={styles.favlistView}>
            <IIcon name="heart-circle-sharp" size={24} color="red" />
          </TouchableOpacity>
        </View>

        {/* image */}
        <ScrollView>
          <View style={styles.container}>
            {loading ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ActivityIndicator size="large" color={COLORS.primaryColor} />
              </View>
            ) : (
              <View style={{marginTop: 30}}>
                <FlatList
                  data={DATA}
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  renderItem={({item}) => (
                    <View key={item.name}>
                      <TouchableOpacity
                        onPress={() => navigation.navigate('HotelStackScreen')}
                        style={styles.icons}
                        source={item.image}>
                        {item.FIcon == true ? (
                          <FIcon
                            name="hotel"
                            size={30}
                            color="#F15F5F"
                            style={{alignSelf: 'center', top: 15}}
                          />
                        ) : null}

                        {item.MIcon == true ? (
                          <MIcon
                            name="home"
                            size={40}
                            color="#F15F5F"
                            style={{alignSelf: 'center', top: 10}}
                          />
                        ) : null}

                        {item.FAIcon == true ? (
                          <FIcon
                            name="warehouse"
                            size={30}
                            color="#F15F5F"
                            style={{alignSelf: 'center', top: 15}}
                          />
                        ) : null}

                        {item.MAIcon == true ? (
                          <MAIcon
                            name="home-city"
                            size={40}
                            color="#F15F5F"
                            style={{alignSelf: 'center', top: 10}}
                          />
                        ) : null}
                      </TouchableOpacity>
                      <Text style={styles.subText}>{item.subtitle}</Text>
                    </View>
                  )}
                />
              </View>
            )}

            <View style={{marginTop: 20, paddingTop: 20}}>
              {/* recomended */}
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Text style={styles.recomendedTxt}>Recomended</Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate('HotelStackScreen')}
                  style={{flex: 0.3}}>
                  <Text style={styles.moreTxt}>More</Text>
                </TouchableOpacity>
              </View>
              {/* imagebox */}
              <FlatList
                data={resorts}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('Description', {
                        passitem: item,
                        passtitle: item.title,
                        passprice: item.price,
                        passimage: item.image,
                      })
                    }
                    style={styles.flatlistView}
                    activeOpacity={0.7}>
                    <Image
                      source={{uri: item.image}}
                      resizeMode={'cover'}
                      style={styles.flatlistImageView}
                    />
                    <View>
                      <View style={styles.titleView}>
                        <Text style={styles.titleTxt}>{item.title}</Text>
                        <Text style={styles.priceTxt}>{item.price}</Text>
                      </View>
                    </View>

                    <View style={styles.locationTxt}>
                      <IIcon
                        flex={0.3}
                        name="location-sharp"
                        size={22}
                        color="grey"
                      />
                      <Text
                        style={{
                          flex: 1,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.placetext}
                      </Text>
                    </View>

                    <View style={styles.bedView}>
                      <IIcon flex={0.3} name="bed" size={22} color="grey" />
                      <Text
                        style={{
                          flex: 0.6,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.bedtext}
                      </Text>

                      <MIcon flex={0.3} name="bathtub" size={22} color="grey" />
                      <Text
                        style={{
                          flex: 0.6,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.bathtext}
                      </Text>
                      <MAIcon
                        flex={0.3}
                        name="arrow-collapse-all"
                        size={22}
                        color="grey"
                      />
                      <Text
                        style={{
                          flex: 0.8,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.sqft}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>

            {/* toprated */}

            <View style={{marginTop: 20, paddingTop: 20, marginBottom: 60}}>
              {/* recomended */}
              <View style={{flex: 1, flexDirection: 'row'}}>
                <Text style={styles.recomendedTxt}>Top Rated</Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate('HotelStackScreen')}
                  style={{flex: 0.3}}>
                  <Text style={styles.moreTxt}>More</Text>
                </TouchableOpacity>
              </View>
              {/* imagebox */}
              <FlatList
                data={toprated}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('Description', {
                        passtitle: item.title,
                        passprice: item.price,
                        passimage: item.image,
                      })
                    }
                    style={styles.flatlistView}
                    activeOpacity={0.7}>
                    <Image
                      source={{uri: item.image}}
                      resizeMode={'cover'}
                      style={styles.flatlistImageView}
                    />
                    <View>
                      <View style={styles.titleView}>
                        <Text style={styles.titleTxt}>{item.title}</Text>
                        <Text style={styles.priceTxt}>{item.price}</Text>
                      </View>
                    </View>

                    <View style={styles.locationTxt}>
                      <IIcon
                        flex={0.3}
                        name="location-sharp"
                        size={22}
                        color="grey"
                      />
                      <Text
                        style={{
                          flex: 1,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.placetext}
                      </Text>
                    </View>

                    <View style={styles.bedView}>
                      <IIcon flex={0.3} name="bed" size={22} color="grey" />
                      <Text
                        style={{
                          flex: 0.6,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.bedtext}
                      </Text>

                      <MIcon flex={0.3} name="bathtub" size={22} color="grey" />
                      <Text
                        style={{
                          flex: 0.6,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.bathtext}
                      </Text>
                      <MAIcon
                        flex={0.3}
                        name="arrow-collapse-all"
                        size={22}
                        color="grey"
                      />
                      <Text
                        style={{
                          flex: 0.8,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.sqft}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>
        </ScrollView>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  headerView: {
    marginTop: '10@vs',
    flexDirection: 'row',
    height: '40@vs',
  },
  headerTxt: {
    fontSize: 20,
    fontFamily: 'Poppins-SemiBold',
    marginLeft: '60@s',
    marginBottom: '10@vs',
    color: '#000',
    textAlign: 'center',
  },
  favlistView: {
    height: '34@vs',
    width: '34@s',
    marginLeft: '60@s',
    borderWidth: '1@s',
    borderRadius: '10@msr',
    justifyContent: 'center',
    borderColor: '#c9c8ce',
    paddingLeft: '5@s',
  },
  flatlistImageView: {
    height: '200@vs',
    width: '310@s',

    borderTopLeftRadius: '20@msr',
    borderTopRightRadius: '20@msr',
  },
  recomendedTxt: {
    flex: 0.8,
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
    paddingLeft: '20@s',
  },
  moreTxt: {
    fontSize: 16,
    fontFamily: 'Poppins-SemiBold',
    color: 'grey',
    paddingLeft: '20@s',
  },
  inputTxt: {
    ...FONTS.h5,
    height: '50@vs',
    width: '320@s',
    alignSelf: 'center',
    backgroundColor: '#fff',
    color: COLORS.blackColor,
    borderColor: COLORS.lightGrey,
    borderRadius: '30@msr',
    paddingStart: '10@msr',
  },
  fblogoStyle: {
    height: '30@vs',
    width: '30@s',
    marginLeft: '10@s',
  },
  mainimage: {
    width: '310@s',
    height: '200@vs',
    resizeMode: 'contain',
  },
  box: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    marginVertical: '10@vs',
    height: '80@vs',
    width: '100@s',
    borderRadius: '10@msr',
    borderWidth: '1@msr',
    borderColor: '#F5B5A3',
  },
  icons: {
    height: '60@vs',
    width: '60@s',
    margin: '5@msr',
    borderRadius: '20@msr',
    backgroundColor: '#DCDCDC',
    marginLeft: '20@s',
  },

  scrollView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flatlistView: {
    borderBottomLeftRadius: '20@msr',
    borderBottomRightRadius: '20@msr',
    borderTopLeftRadius: '20@msr',
    borderTopRightRadius: '20@msr',
    margin: '10@msr',

    alignSelf: 'center',
    marginTop: '30@vs',
    borderRadius: '3@msr',
    width: '310@s',
    backgroundColor: COLORS.whiteColor,
    shadowColor: COLORS.blackColor,
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: '10@msr',
    elevation: 5,
    bottom: '5@vs',
  },
  titleView: {
    flexDirection: 'row',
    paddingLeft: '20@s',
    marginTop: '10@vs',
    flexDirection: 'row',
  },
  titleTxt: {
    flex: 0.8,
    fontSize: 18,
    // fontWeight: '600',
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  priceTxt: {
    flex: 0.7,
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
    color: '#F15F5F',
  },
  locationTxt: {
    flexDirection: 'row',
    margin: '5@msr',
    flexDirection: 'row',
    paddingLeft: '10@s',
  },
  bedView: {
    flexDirection: 'row',
    flexDirection: 'row',
    paddingLeft: '15@s',
    paddingRight: '10@s',
    paddingBottom: '10@vs',
  },
  subText: {
    fontSize: 16,
    marginLeft: '15@s',
    textAlign: 'center',
    fontWeight: '300',
    color: 'black',
    fontFamily: 'Poppins-Regular',
  },
});

export default Home;
