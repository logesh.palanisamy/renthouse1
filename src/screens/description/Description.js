import React, {useContext, useState} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  Linking,
  Platform,
} from 'react-native';
import Mailer from 'react-native-mail';
import {COLORS, FONTS, Vector, Images, PlaceHolders} from '../../constants';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import MAIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import IIcon from 'react-native-vector-icons/Ionicons';
import EIcon from 'react-native-vector-icons/Entypo';
import FIcon from 'react-native-vector-icons/FontAwesome5';
import {ScaledSheet} from 'react-native-size-matters';
import {ScrollView} from 'react-native-gesture-handler';
import ReadMore from '@fawazahmed/react-native-read-more';
import {SafeAreaView} from 'react-native-safe-area-context';
import {AuthContext} from '../../navigation/AuthProvider';
import database from '@react-native-firebase/database';
import AIcon from 'react-native-vector-icons/AntDesign';
import ImageView from 'react-native-image-viewing';

const Description = ({navigation, route}) => {
  const {passprice, passtitle, passimage, passitem} = route.params;

  const DATA = [
    {
      name: '1',
      image: require('../../../assets/images/room1.jpg'),
    },
    {
      name: '2',
      image: require('../../../assets/images/room2.jpg'),
    },
    {
      name: '3',
      image: require('../../../assets/images/room3.jpg'),
    },
    {
      name: '4',
      image: require('../../../assets/images/room4.jpg'),
    },
    {
      name: '4',
      image: require('../../../assets/images/room1.jpg'),
    },
  ];

  const images = [
    {
      uri: 'https://images.unsplash.com/photo-1560448205-4d9b3e6bb6db?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    },
    {
      uri: 'https://images.unsplash.com/photo-1522708323590-d24dbb6b0267?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    },
    {
      uri: 'https://images.unsplash.com/photo-1622429420441-60dd67f737a6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    },
    {
      uri: 'https://images.unsplash.com/photo-1560185009-dddeb820c7b7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    },
    {
      uri: 'https://images.unsplash.com/photo-1613553474179-e1eda3ea5734?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
    },
  ];

  const [visible, setIsVisible] = useState(false);

  dialCall = () => {
    let phoneNumber = '';

    if (Platform.OS === 'android') {
      phoneNumber = 'tel:${8681849103}';
    } else {
      phoneNumber = 'telprompt:${8681849103}';
    }

    Linking.openURL(phoneNumber);
  };

  // email
  const Booknow = () => {
    Mailer.mail(
      {
        subject: `Resort Total Amount - ${passprice}`,
        recipients: ['logeshsiam@gmail.com'],
        body: `<p>Hey, Congratulations On Your New Booking our Resort . Hope you get all that you are looking for Joy.</p>`,
        isHTML: true,
      },
      (error, event) => {
        Alert.alert(
          error,
          event,
          [
            {
              text: 'Ok',
              onPress: () => console.log('OK: Email Error Response'),
            },
            {
              text: 'Cancel',
              onPress: () => console.log('CANCEL: Email Error Response'),
            },
          ],
          {cancelable: true},
        );
      },
    );
  };

  // favor

  const {user, logout} = useContext(AuthContext);
  const addAction = () => {
    favItem.push(passitem);

    setFav(true);

    //console.log('action', item)
  };
  let favItem = database().ref(`/users/${user.uid}/favoritems`);

  const [Fav, setFav] = useState(false);

  return (
    <ScrollView style={styles.container}>
      <SafeAreaView>
        {/* header */}
        <View
          style={{
            paddingLeft: 15,
            marginTop: 10,
            flexDirection: 'row',
            height: 40,
          }}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Homescreen')}
            style={{
              height: 34,
              width: 34,
              borderWidth: 1,
              borderRadius: 10,
              justifyContent: 'center',
              borderColor: '#c9c8ce',
            }}>
            <IIcon name="chevron-back" size={24} color="#000" />
          </TouchableOpacity>
          <View style={{alignSelf: 'center'}}>
            <Text
              style={{
                fontSize: 18,
                fontFamily: 'Poppins-SemiBold',
                marginLeft: 100,
                color: '#000',
              }}>
              Details
            </Text>
          </View>
        </View>

        {/* slider */}

        <View onLayout={this.onLayout}>
          <Image
            source={{uri: passimage}}
            resizeMode={'cover'}
            style={styles.buyImage}
          />
        </View>

        {/* image */}
        <View>
          <View style={styles.resortImage}>
            <Text
              style={{
                flex: 0.6,
                fontSize: 18,
                fontFamily: 'Poppins-SemiBold',
                color: '#000',
              }}>
              {passtitle}
            </Text>
            <Text
              style={{
                flex: 0.4,
                textAlign: 'right',
                fontSize: 18,
                fontFamily: 'Poppins-SemiBold',
                color: '#F15F5F',
              }}>
              {passprice}
            </Text>
          </View>

          {/* location */}

          <View style={styles.locationView}>
            <IIcon flex={0.3} name="location-sharp" size={22} color="grey" />
            <Text
              style={{
                flex: 0.9,
                fontSize: 16,
                color: '#6f6f6f',
                fontFamily: 'Poppins-Regular',
              }}>
              1691 Old Bayshore Hwy
            </Text>

            {/* fav */}

            <TouchableOpacity onPress={() => addAction()}>
              {Fav ? (
                <AIcon name="heart" size={22} color="red" />
              ) : (
                <AIcon
                  style={{marginLeft: 15, marginBottom: 2}}
                  name="hearto"
                  size={22}
                  color="red"
                />
              )}
            </TouchableOpacity>
          </View>

          {/* beds */}

          <View style={styles.bedView}>
            <IIcon flex={0.3} name="bed" size={22} color="grey" />
            <Text
              style={{
                flex: 0.7,
                fontSize: 16,
                color: '#6f6f6f',
                fontFamily: 'Poppins-Regular',
              }}>
              {' '}
              3 Beds{' '}
            </Text>

            <MIcon flex={0.3} name="bathtub" size={22} color="grey" />
            <Text
              style={{
                flex: 0.7,
                fontSize: 16,
                color: '#6f6f6f',
                fontFamily: 'Poppins-Regular',
              }}>
              {' '}
              4 Baths{' '}
            </Text>
            <MAIcon
              flex={0.3}
              name="arrow-collapse-all"
              size={22}
              color="grey"
            />
            <Text
              style={{
                flex: 0.7,
                fontSize: 16,
                color: '#6f6f6f',
                fontFamily: 'Poppins-Regular',
              }}>
              {' '}
              2450 sqft{' '}
            </Text>
          </View>

          {/*    Description */}

          <View style={styles.titleView}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: '600',
                color: '#000',
                fontFamily: 'Poppins-SemiBold',
              }}>
              Description
            </Text>

            <ReadMore
              numberOfLines={5}
              style={{
                fontSize: 14,
                margin: 5,
                color: '#6f6f6f',
                fontFamily: 'Poppins-Regular',
              }}>
              {
                ' A hotel an establishment that provides paid lodging on a short-term basis. Facilities provided inside a hotel room may range from a modest-quality mattress in a small room to large suites with bigger, higher-quality beds, a dresser, a refrigerator and other kitchen facilities, upholstered chairs, a flat screen television,and en-suite bathrooms'
              }
            </ReadMore>
          </View>

          {/* owner call */}
          <View style={styles.titleView}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: '600',
                color: '#000',
                fontFamily: 'Poppins-SemiBold',
              }}>
              Contact Resort
            </Text>
            <View style={{flexDirection: 'row', marginTop: 10, marginLeft: 10}}>
              <Text
                style={{
                  flex: 0.6,
                  fontSize: 18,
                  // fontWeight: '300',
                  fontFamily: 'Poppins-Regular',
                  color: '#6f6f6f',
                }}>
                Property Owner
              </Text>
              <TouchableOpacity onPress={this.dialCall}>
                <FIcon name="phone-alt" size={22} color="#F15F5F" />
              </TouchableOpacity>
            </View>
          </View>

          {/* gallery */}
          <View style={styles.titleView}>
            <Text
              style={{
                fontSize: 18,
                fontWeight: '600',
                color: '#000',
                fontFamily: 'Poppins-SemiBold',
              }}>
              Gallery
            </Text>
            <View style={{flexDirection: 'row', marginTop: 10}}>
              <ImageView
                images={images}
                imageIndex={0}
                visible={visible}
                onRequestClose={() => setIsVisible(false)}
              />

              <FlatList
                data={images}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    key={item.name}
                    onPress={() => setIsVisible(true)}>
                    <Image style={styles.icons} source={{uri: item.uri}} />
                  </TouchableOpacity>
                )}
              />
            </View>
          </View>

          {/* addon */}

          <View
            style={{
              flex: 1,
              marginTop: 20,
              paddingLeft: 20,
            }}>
            <View>
              <Text style={styles.sideTitle}>Home facilities</Text>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <EIcon name="air" size={22} color="grey" />
                <Text style={styles.facilities}>Air conditioner</Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <IIcon name="fast-food" size={22} color="grey" />
                <Text style={styles.facilities}>Kitchen</Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <FIcon name="parking" size={22} color="grey" />
                <Text style={styles.facilities}>Free Parking</Text>
              </View>
              <View style={{flexDirection: 'row', marginTop: 10}}>
                <FIcon name="wifi" size={22} color="grey" />
                <Text style={styles.facilities}>Free Wifi</Text>
              </View>
            </View>
          </View>
        </View>

        {/* rentnow */}

        <View style={{margin: 20, alignSelf: 'center', marginBottom: 20}}>
          <TouchableOpacity onPress={Booknow} style={styles.primaryButtonView}>
            <Text
              style={{
                color: COLORS.whiteColor,
                fontSize: 18,
                fontFamily: 'Poppins-SemiBold',
                textAlign: 'center',
              }}>
              Rent Now
            </Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  inputTxt: {
    ...FONTS.h5,
    height: '50@vs',
    width: '320@s',
    alignSelf: 'center',
    backgroundColor: '#fff',
    color: COLORS.blackColor,
    borderColor: COLORS.lightGrey,
    borderRadius: '30@msr',
    paddingStart: '10@msr',
  },
  fblogoStyle: {
    height: '30@vs',
    width: '30@s',
    marginLeft: '10@s',
  },
  mainimage: {
    width: '310@s',
    height: '200@vs',
    resizeMode: 'contain',
    borderRadius: '10@msr',
  },
  box: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    marginVertical: '10@vs',
    height: '80@vs',
    width: '100@s',
    borderRadius: '10@msr',
    borderWidth: '1@msr',
    borderColor: '#F5B5A3',
  },
  icons: {
    height: '70@vs',
    width: '80@s',
    margin: '5@msr',
    borderRadius: '10@msr',
    backgroundColor: 'pink',
  },
  buyImage: {
    height: '200@vs',
    width: '330@s',
    alignSelf: 'center',
    borderRadius: '10@msr',
    marginTop: '10@vs',
  },
  facilities: {
    marginLeft: '10@s',
    fontSize: 16,
    fontFamily: 'Poppins-Regular',
    color: '#6f6f6f',
  },
  primaryButtonView: {
    paddingVertical: '10@vs',
    width: '250@s',
    backgroundColor: COLORS.primaryColor,
    borderRadius: '10@msr',
  },
  resortImage: {
    flexDirection: 'row',
    paddingLeft: '15@s',
    paddingRight: '20@s',
    flexDirection: 'row',
    marginTop: '20@vs',
  },
  locationView: {
    flexDirection: 'row',
    margin: '5@msr',
    flexDirection: 'row',
    paddingLeft: '10@s',
    paddingRight: '20@s',
    marginTop: '10@vs',
  },
  bedView: {
    flexDirection: 'row',
    flexDirection: 'row',
    paddingLeft: '15@s',
    paddingRight: '20@s',
  },
  titleView: {
    paddingLeft: '15@s',
    paddingRight: '15@s',
    marginTop: '15@vs',
  },
  sideTitle: {
    flex: 0.6,
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
});

export default Description;
