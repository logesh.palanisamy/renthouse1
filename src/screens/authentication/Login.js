import React, {useRef, useState, useEffect} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  SafeAreaView,
} from 'react-native';
import {COLORS, FONTS, Vector} from '../../constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ScaledSheet} from 'react-native-size-matters';
import auth from '@react-native-firebase/auth';
import {validateEmail} from '../../utils';
import SpinnerButton from 'react-native-spinner-button';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [error1, setError1] = useState(false);
  const [error2, setError2] = useState(false);

  const [errorText1, setErrorText1] = useState(false);
  const [errorText2, setErrorText2] = useState(false);

  const input1 = useRef();
  const input2 = useRef();

  const [secureTextEntry, setSecureTextEntry] = React.useState(true);

  const UpdatePasswordVisiblity = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const login = async () => {
    if (email === '') {
      Alert.alert('Email field cannot be Empty!');
    } else if (!validateEmail(email)) {
      Alert.alert('Email Address is invalid');
    } else if (password === '') {
      Alert.alert('Password is required');
    } else {
      setLoading(true);
      try {
        await auth()
          .signInWithEmailAndPassword(email, password)
          .then(() => {
            setLoading(false);
          });
      } catch (error) {
        if (error.code === 'auth/wrong-password') {
          console.log('E-mail address/Password is incorrect!');
          Alert.alert('E-mail address/Password is incorrect!');
        }
        if (error.code === 'auth/user-not-found') {
          console.log('E-mail is not registered!');
          Alert.alert('E-mail is not registered!');
        }
      }
      setLoading(false);
    }
  };

  // loader

  const [loading, setLoading] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <View style={{paddingLeft: 20, marginTop: 20, height: 40}}>
        <TouchableOpacity onPress={() => navigation.navigate('Slider')}>
          <Icon name="arrow-back" size={30} color="#000" />
        </TouchableOpacity>
      </View>
      <View style={{marginTop: 20}}>
        <Text style={styles.topictext}>Welcome Back!</Text>
        <Text style={styles.subtopictext}>
          Log In to your Placoo account to explore your dream{'\n'}place to live
          across the whole world!
        </Text>
      </View>
      <View>
        <Text style={styles.emailtext}>Email ID</Text>
        <View style={styles.textinputview}>
          <TextInput
            style={styles.inputTxt}
            onChangeText={text => {
              setEmail(text);
              setError1(false);
              setErrorText1(false);
            }}
            value={email}
            selectionColor={COLORS.primaryColor}
            placeholder="Enter your Email"
            placeholderTextColor={COLORS.darkGrey}
            keyboardType="email-address"
            borderWidth={1}
            returnKeyType="next"
            onSubmitEditing={() => {
              input2.current.focus();
            }}
            blurOnSubmit={false}
            ref={input1}
            error={error1}
            errorText={errorText1}
          />
        </View>
      </View>

      <View>
        <Text style={styles.emailtext}>Password</Text>
        <View style={styles.textinputview}>
          <View>
            <TextInput
              style={styles.inputTxt}
              onChangeText={text => {
                setPassword(text);
                setError2(false);
                setErrorText2(false);
              }}
              value={password}
              placeholder="Enter Your Password"
              placeholderTextColor={COLORS.darkGrey}
              selectionColor={COLORS.primaryColor}
              keyboardType="default"
              maxLength={8}
              borderWidth={1}
              returnKeyType="done"
              ref={input2}
              error={error2}
              errorText={errorText2}
              secureTextEntry={secureTextEntry ? true : false}
            />
            <TouchableOpacity
              onPress={() => UpdatePasswordVisiblity()}
              style={styles.eyeIcon}>
              {secureTextEntry ? Vector.EyeOff : Vector.Eye}
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <SpinnerButton
        buttonStyle={styles.primarybutton}
        isLoading={loading}
        onPress={() => login(email, password)}
        spinnerType={'MaterialIndicator'}>
        <Text style={styles.primarybuttontext}>Log in</Text>
      </SpinnerButton>

      {/* SIGNUP */}

      <View style={{marginTop: 50, flexDirection: 'row', alignSelf: 'center'}}>
        <Text> Do you have an account ?</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
          <Text
            style={{
              color: COLORS.primaryColor,
              fontFamily: 'Poppins-SemiBold',
            }}>
            {' '}
            Sign up
          </Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  inputTxt: {
    ...FONTS.h5,
    height: '50@vs',
    width: '300@s',
    alignSelf: 'center',
    backgroundColor: '#fff',
    color: COLORS.blackColor,
    borderColor: COLORS.lightGrey,
    borderRadius: '10@msr',
    paddingStart: '10@msr',
  },
  fblogoStyle: {
    height: '30@vs',
    width: '30@s',
    marginLeft: '10@s',
  },
  topictext: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 24,
    paddingLeft: '20@s',
    color: 'black',
  },
  subtopictext: {
    fontSize: 12,
    paddingLeft: '23@s',
    marginTop: '10@vs',
    color: 'grey',
    fontFamily: 'Poppins-Medium',
  },
  emailtext: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    paddingLeft: '20@s',
    marginTop: '20@vs',
    color: 'black',
  },
  textinputview: {
    marginTop: '10@vs',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '10@msr',
    marginBottom: '10@vs',
  },
  primarybutton: {
    paddingVertical: '10@vs',
    width: '300@s',
    backgroundColor: COLORS.primaryColor,
    borderRadius: '10@msr',
    alignSelf: 'center',
    marginTop: '50@vs',
  },
  primarybuttontext: {
    color: COLORS.whiteColor,
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Poppins-SemiBold',
  },
  eyeIcon: {
    position: 'absolute',
    top: '10@vs',
    right: '10@s',
  },
});

export default Login;
