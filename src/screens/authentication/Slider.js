import React, {useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import {ScaledSheet} from 'react-native-size-matters';
import {COLORS, FONTS, Fonts} from '../../constants';

const slides = [
  {
    key: 's1',
    title: 'Great Offers',
    text: 'Enjoy Great offers & services',
    image: require('../../../assets/images/intro5.jpg'),
  },

  {
    key: 's2',
    title: 'Grab Soon',
    text: 'Upto 25% off on 5 Star Resorts',
    image: require('../../../assets/images/intro6.jpg'),
  },
  {
    key: 's3',
    title: 'Great Offers',
    text: 'Enjoy Great offers on our all services',
    image: require('../../../assets/images/intro7.jpg'),
  },
];

const Slider = ({navigation}) => {
  const [showRealApp, setShowRealApp] = useState(false);

  const onDone = () => {
    setShowRealApp(true);
  };

  const renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Text
          style={{
            textAlign: 'center',
            fontWeight: '600',
            color: '#fff',
            fontFamily: 'Poppins-Regular',
          }}>
          Get Started
        </Text>
      </View>
    );
  };

  const RenderItem = ({item}) => {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          resizeMode: 'contain',
        }}>
        <Image style={styles.introImageStyle} source={item.image} />
        <Text style={styles.introTitleStyle}>{item.title}</Text>
        <Text style={styles.introTextStyle}>{item.text}</Text>
      </View>
    );
  };

  return (
    <>
      {showRealApp ? (
        <SafeAreaView style={styles.container}>
          <ImageBackground
            style={styles.introImageStyle}
            source={require('../../../assets/images/intro9.jpg')}
          />
          <View style={styles.container1}>
            <Text style={styles.titleStyle}>New Place, New Home!</Text>
            <Text style={styles.paragraphStyle}>
              Are you ready to uproot and start over in a new area? Placoo will
              help you on your journey!
            </Text>

            <TouchableOpacity
              style={styles.primarybutton}
              onPress={() => navigation.navigate('Login')}>
              <Text style={styles.primarybuttontext}>Log in</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.whitebutton}
              onPress={() => navigation.navigate('Signup')}>
              <Text style={styles.whitebuttontext}>Sign up</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      ) : (
        <AppIntroSlider
          data={slides}
          renderItem={RenderItem}
          onDone={onDone}
          dotStyle={{backgroundColor: '#d5e2e3'}}
          activeDotStyle={{backgroundColor: '#F15F5F'}}
          showNextButton={false}
          renderDoneButton={renderDoneButton}
        />
      )}
    </>
  );
};

export default Slider;

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container1: {
    alignItems: 'center',
    justifyContent: 'center',
    bottom: '20@s',
  },

  titleStyle: {
    padding: '5@msr',
    textAlign: 'center',
    fontSize: 20,
    color: '#000',
    fontFamily: 'Poppins-Regular',
  },
  paragraphStyle: {
    padding: 5,
    textAlign: 'center',
    fontSize: 16,
    color: '#000',
    fontFamily: 'Poppins-Regular',
  },
  introImageStyle: {
    flex: 1,
    width: '600@s',
    height: '900@vs',
    resizeMode: 'contain',
  },
  introTextStyle: {
    fontSize: 18,
    color: COLORS.primaryColor,
    textAlign: 'center',
    paddingVertical: '40@vs',
    position: 'absolute',
    marginTop: '510@vs',
    fontWeight: '500',
    fontFamily: 'Poppins-Regular',
  },
  introTitleStyle: {
    fontSize: 25,
    color: '#000',
    textAlign: 'center',
    marginBottom: 16,
    position: 'absolute',
    marginTop: '500@vs',
    fontFamily: 'Poppins-Regular',
  },
  whitebutton: {
    margin: '10@msr',
    paddingVertical: '10@vs',
    width: '300@s',
    backgroundColor: COLORS.whiteColor,
    borderRadius: '10@msr',
  },
  primarybutton: {
    paddingVertical: '10@vs',
    width: '300@s',
    backgroundColor: COLORS.primaryColor,
    borderRadius: '10@msr',
  },
  primarybuttontext: {
    color: COLORS.whiteColor,
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
  },
  whitebuttontext: {
    color: COLORS.primaryColor,
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
  },
  buttonCircle: {
    height: '40@vs',
    width: '100@s',
    backgroundColor: COLORS.primaryColor,
    justifyContent: 'center',
    borderRadius: '10@msr',
  },
});
