import React, {useRef, useState} from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  SafeAreaView,
} from 'react-native';

import {COLORS, FONTS, Vector} from '../../constants';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {ScaledSheet} from 'react-native-size-matters';
import {ScrollView} from 'react-native-gesture-handler';
import PhoneInput from 'react-native-phone-number-input';
import auth from '@react-native-firebase/auth';
import {allowNumOnly, validateEmail} from '../../utils';
import database from '@react-native-firebase/database';
import SpinnerButton from 'react-native-spinner-button';

const Signup = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState(0);

  const [error1, setError1] = useState(false);
  const [error2, setError2] = useState(false);
  const [error3, setError3] = useState(false);

  const [errorText1, setErrorText1] = useState(false);
  const [errorText2, setErrorText2] = useState(false);
  const [errorText3, setErrorText3] = useState(false);

  const input1 = useRef();
  const input2 = useRef();
  const input3 = useRef();

  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const UpdatePasswordVisiblity = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const register = async () => {
    if (email === '') {
      Alert.alert('Email field cannot be Empty!');
    } else if (!validateEmail(email)) Alert.alert('E-mail address is Invalid');
    else if (password === '') {
      Alert.alert('Password is required');
    } else if (phoneNumber === '') {
      Alert.alert('Phonenumber is required');
    } else if (!allowNumOnly(phoneNumber)) {
    } else {
      setLoading(true);
      try {
        await auth()
          .createUserWithEmailAndPassword(email, password)
          .then(() => {
            const reference = database()
              .ref(`/users/${auth().currentUser.uid}`)
              .set({
                email: email,
                phoneNumber: phoneNumber,
              });
            setLoading(false);
          });
      } catch (error) {
        if (error.code === 'auth/email-already-in-use') {
          console.log('E-mail address is already in use!');
          Alert.alert('E-mail address is already in use!');
        }
        if (error.code === 'auth/password-already-in-use') {
          console.log('password is already in use!');
          Alert.alert('password is already in use!');
        }
        if (error.code === 'auth/phoneNumber-already-in-use') {
          console.log('phoneNumberis already in use!');
          Alert.alert('phoneNumber is already in use!');
        }
      }
      setLoading(false);
    }
  };

  const [loading, setLoading] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <View style={{paddingLeft: 20, marginTop: 20, height: 40}}>
        <TouchableOpacity onPress={() => navigation.navigate('Slider')} s>
          <Icon name="arrow-back" size={30} color="#000" />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={{marginTop: 30}}>
          <Text style={styles.loginTitle}>Let’s explore together!</Text>
          <Text style={styles.loginSubTitle}>
            Create your Placoo account to explore your dream place{'\n'}to live
            across the whole world!
          </Text>
        </View>

        {/* emailid */}

        <View>
          <Text style={styles.emailIdTxt}>Email ID</Text>

          <View style={styles.txtInputView}>
            <TextInput
              style={styles.inputTxt}
              onChangeText={text => {
                setEmail(text);
                setError1(false);
                setErrorText1(false);
              }}
              value={email}
              borderRadius={10}
              placeholder="Enter your Email"
              selectionColor={COLORS.primaryColor}
              placeholderTextColor={COLORS.darkGrey}
              keyboardType="email-address"
              borderWidth={1}
              returnKeyType="next"
              onSubmitEditing={() => {
                input2.current.focus();
              }}
              blurOnSubmit={false}
              ref={input1}
              error={error1}
              errorText={errorText1}
            />
          </View>
        </View>

        {/* password */}

        <View>
          <Text style={styles.emailIdTxt}>Password</Text>

          <View style={styles.txtInputView}>
            <View>
              <TextInput
                style={styles.inputTxt}
                borderRadius={10}
                onChangeText={text => {
                  setPassword(text);
                  setError2(false);
                  setErrorText2(false);
                }}
                value={password}
                placeholder="Enter Your Password"
                selectionColor={COLORS.primaryColor}
                placeholderTextColor={COLORS.darkGrey}
                keyboardType="default"
                borderWidth={1}
                maxLength={8}
                returnKeyType="next"
                blurOnSubmit={false}
                ref={input2}
                error={error2}
                errorText={errorText2}
                secureTextEntry={secureTextEntry ? true : false}
              />
              <TouchableOpacity
                onPress={() => UpdatePasswordVisiblity()}
                style={styles.eyeIcon}>
                {secureTextEntry ? Vector.EyeOff : Vector.Eye}
              </TouchableOpacity>
            </View>
          </View>
        </View>

        {/* phonenumber */}
        <View>
          <Text style={styles.emailIdTxt}>Phone number</Text>
          <TouchableOpacity style={styles.txtInputView}>
            <PhoneInput
              onChangeText={text => {
                setPhoneNumber(text);
                setError3(false);
                setErrorText3(false);
              }}
              value={phoneNumber}
              placeholder="Enter your Phone no"
              containerStyle={styles.phoneNumberView}
              textContainerStyle={{paddingVertical: 0}}
              returnKeyType="done"
              keyboardType="number-pad"
              ref={input3}
              error={error3}
              errorText={errorText3}
              maxLength={10}
            />
          </TouchableOpacity>
        </View>

        {/* create account */}

        <SpinnerButton
          buttonStyle={styles.primaryButtonView}
          isLoading={loading}
          onPress={() => register(email, password, phoneNumber)}
          spinnerType={'MaterialIndicator'}>
          <Text style={styles.secondaryTxt}>Sign Up</Text>
        </SpinnerButton>

        <View
          style={{marginTop: 50, flexDirection: 'row', alignSelf: 'center'}}>
          <Text> Already have an account ?</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text
              style={{
                color: COLORS.primaryColor,
                fontFamily: 'Poppins-SemiBold',
              }}>
              {' '}
              Login
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
  },
  inputTxt: {
    ...FONTS.h5,
    height: '50@vs',
    width: '300@s',
    alignSelf: 'center',
    backgroundColor: '#fff',
    color: COLORS.blackColor,
    borderColor: COLORS.lightGrey,
    paddingStart: '10@msr',
  },
  socialIcon: {
    height: '30@vs',
    width: '30@s',
    marginLeft: '10@s',
  },
  phoneNumberView: {
    width: '300@s',
    height: '50@vs',
    borderColor: COLORS.lightGrey,
    borderWidth: '1@s',
    borderRadius: '10@msr',
    paddingRight: '25@s',
  },
  eyeIcon: {
    position: 'absolute',
    top: '10@vs',
    right: '10@s',
  },
  loginTitle: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 24,
    paddingLeft: '20@s',
    color: 'black',
  },
  loginSubTitle: {
    fontFamily: 'Poppins-Medium',
    fontSize: 12,
    paddingLeft: '20@s',
    marginTop: '20@vs',
  },
  emailIdTxt: {
    fontFamily: 'Poppins-SemiBold',
    fontSize: 16,
    paddingLeft: '20@s',
    marginTop: '20@vs',
    color: 'black',
  },
  txtInputView: {
    marginTop: '10@vs',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '10@vs',
  },
  primaryButtonView: {
    paddingVertical: '10@vs',
    width: '300@s',
    backgroundColor: COLORS.primaryColor,
    borderRadius: '15@msr',
    alignSelf: 'center',
    marginTop: '20@vs',
  },
  secondaryTxt: {
    color: COLORS.whiteColor,
    textAlign: 'center',
    fontFamily: 'Poppins-SemiBold',
    fontSize: 18,
  },
});

export default Signup;
