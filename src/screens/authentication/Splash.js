import React, {useEffect} from 'react';
import {View, Image, SafeAreaView} from 'react-native';
const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('Slider');
    }, 2000);
  }, []);
  return (
    <SafeAreaView>
      <Image
        style={{height: 825, resizeMode: 'contain', alignSelf: 'center'}}
        source={require('../../../assets/images/splash.jpg')}></Image>
    </SafeAreaView>
  );
};
export default Splash;
