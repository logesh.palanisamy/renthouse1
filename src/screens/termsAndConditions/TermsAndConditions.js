import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import IIcon from 'react-native-vector-icons/Ionicons';
import {FONTS, COLORS} from '../../constants/Theme';
import {WebView} from 'react-native-webview';
const TermsAndConditions = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 700);
  }, []);

  const [loading, setLoading] = useState(true);

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        {/* header */}
        <View
          style={{
            paddingLeft: 10,
            margin: 10,
            flexDirection: 'row',
            height: 40,
          }}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Homescreen')}
            style={{
              height: 34,
              width: 34,
              borderWidth: 1,
              borderRadius: 10,
              justifyContent: 'center',
              borderColor: '#c9c8ce',
            }}>
            <IIcon name="chevron-back" size={24} color="#000" />
          </TouchableOpacity>

          <View style={{justifyContent: 'center', paddingLeft: 60}}>
            <Text style={{fontSize: 18, fontWeight: '600', color: '#000'}}>
              Terms And Conditions
            </Text>
          </View>
        </View>

        {loading ? (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator size="large" color={COLORS.primaryColor} />
          </View>
        ) : (
          <>
            <WebView
              source={{
                uri: 'https://www.termsandconditionsgenerator.com/live.php?token=EehHBdWMgNXgpsUqbWfhjzNOSGtL20ER',
              }}
              hasZoom={false}
              scrollEnabled={false}
            />
          </>
        )}
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
});
export default TermsAndConditions;
