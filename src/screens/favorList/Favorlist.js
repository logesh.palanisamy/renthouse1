import React, {useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import {COLORS} from '../../constants/Theme';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import MAIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import IIcon from 'react-native-vector-icons/Ionicons';
import {ScaledSheet} from 'react-native-size-matters';
import database from '@react-native-firebase/database';
import {AuthContext} from '../../navigation/AuthProvider';

const Favorlist = ({navigation}) => {
  const DATA = [
    {
      name: '2',
      image:
        'https://images.unsplash.com/photo-1596436889106-be35e843f974?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'King House',
      price: '$1200/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '3 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },
    {
      name: '3',
      image:
        'https://images.unsplash.com/photo-1596178067639-5c6e68aea6dc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'Rage Resorts',
      price: '$2060/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '3 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },

    {
      name: '5',
      image:
        'https://images.unsplash.com/photo-1617859047452-8510bcf207fd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
      title: 'God Resorts',
      price: '$1200/month',
      placetext: '1691 Old Bayshore Hwy',
      bedtext: '3 Beds',
      bathtext: '4 Baths',
      sqft: '2450 sqft',
    },
  ];

  // loader
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 700);
  }, []);

  const [loading, setLoading] = useState(true);

  // favor
  const [datas, setDatas] = useState([]);
  const {user, logout} = useContext(AuthContext);

  useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      if (data !== null) {
        const products = Object.values(data);
        setDatas(products);
        // setLoading(false);
        console.log(products);
      } else {
      }
    });
    setLoading(false);
    console.log('datas', datas);
  }, []);

  let itemsRef = database().ref(`/users/${user.uid}/favoritems`);

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color={COLORS.primaryColor} />
        </View>
      ) : (
        <>
          {datas.length !== 0 ? (
            <View>
              <View style={{flexDirection: 'row', height: 50}}>
                <View
                  style={{
                    paddingLeft: 10,
                    margin: 10,
                    flexDirection: 'row',
                  }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Homescreen')}
                    style={styles.backbutton}>
                    <IIcon name="chevron-back" size={24} color="#000" />
                  </TouchableOpacity>

                  <View style={{marginLeft: 100}}>
                    <Text
                      style={{
                        fontSize: 18,
                        fontWeight: '600',
                        color: '#000',
                        fontFamily: 'Poppins-SemiBold',
                      }}>
                      Favourites
                    </Text>
                  </View>
                </View>
              </View>

              <FlatList
                data={datas}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('Description', {
                        passitem: item,
                        passtitle: item.title,
                        passprice: item.price,
                        passimage: item.image,
                      })
                    }
                    style={styles.details}
                    activeOpacity={0.7}>
                    <Image
                      source={{uri: item.image}}
                      resizeMode={'cover'}
                      style={styles.imageview}
                    />
                    <View>
                      <View style={styles.titleView}>
                        <Text style={styles.titleTxt}>{item.title}</Text>
                        <Text
                          style={{
                            flex: 0.6,
                            fontSize: 18,
                            fontFamily: 'Poppins-SemiBold',
                            // fontWeight: '600',
                            color: '#F15F5F',
                          }}>
                          {item.price}
                        </Text>
                      </View>
                    </View>

                    <View style={styles.locationView}>
                      <IIcon
                        flex={0.3}
                        name="location-sharp"
                        size={22}
                        color="grey"
                      />
                      <Text
                        style={{
                          flex: 1,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.placetext}
                      </Text>
                    </View>

                    <View style={styles.thirdRowView}>
                      <IIcon flex={0.3} name="bed" size={22} color="grey" />
                      <Text
                        style={{
                          flex: 0.6,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {item.bedtext}
                      </Text>

                      <MIcon flex={0.3} name="bathtub" size={22} color="grey" />
                      <Text
                        style={{
                          flex: 0.6,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {' '}
                        {item.bathtext}
                      </Text>
                      <MAIcon
                        flex={0.3}
                        name="arrow-collapse-all"
                        size={22}
                        color="grey"
                      />
                      <Text
                        style={{
                          flex: 0.8,
                          fontSize: 16,
                          color: '#6f6f6f',
                          fontFamily: 'Poppins-Regular',
                        }}>
                        {' '}
                        {item.sqft}{' '}
                      </Text>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </View>
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
              }}>
              <View style={{flex: 0.5}}>
                <Image
                  source={require('../../../assets/images/favor.png')}
                  style={{
                    resizeMode: 'contain',
                    alignSelf: 'center',
                    width: '50%',
                    height: '50%',
                  }}></Image>
                <Text
                  style={{
                    fontSize: 26,
                    color: '#000',
                    textAlign: 'center',
                    marginTop: 10,
                  }}>
                  No Favorities Yet
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    color: '#000',
                    textAlign: 'center',
                    marginTop: 10,
                  }}>
                  Click the 'Explore' button to add your favorities
                </Text>

                <TouchableOpacity
                  onPress={() => navigation.navigate('HotelStackScreen')}
                  style={styles.primaryButtonView}>
                  <Text
                    style={{
                      color: '#fff',
                      fontSize: 18,
                      fontWeight: '600',
                      textAlign: 'center',
                    }}>
                    Explore
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </>
      )}
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  container: {
    flex: 1,
    marginBottom: '100@vs',
  },
  backbutton: {
    height: '30@vs',
    width: '30@s',
    borderWidth: '1@msr',
    borderRadius: '10@msr',
    justifyContent: 'center',
    borderColor: '#c9c8ce',
    paddingLeft: '3@msr',
  },
  box: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    marginVertical: '10@vs',
    height: '80@vs',
    width: '350@s',
    alignSelf: 'center',
    borderRadius: '6@msr',
    borderWidth: '1@msr',
    borderColor: '#F5B5A3',
  },
  icons: {
    height: '78@vs',
    width: '70@s',
    borderRadius: '6@msr',
  },
  text1: {
    fontSize: 18,
    fontWeight: '800',
    color: '#000',
    textAlign: 'left',
    width: '200@s',
    left: '20@s',
    top: '10@vs',
    flexDirection: 'column',
  },
  text2: {
    left: '20@s',
    fontSize: 13,
    color: '#a9a9a9',
    fontWeight: '400',
    flexDirection: 'column',
  },

  text3: {
    fontSize: 10,
    textAlign: 'center',
    fontWeight: '700',
    color: '#fff',
    backgroundColor: '#D25151',
    borderRadius: '10@msr',
    width: '40@s',
    height: '15@vs',
    right: '80@s',
    top: '15@vs',
  },

  newtext: {
    fontSize: 13,
    fontWeight: '700',
    right: '90@s',
    backgroundColor: 'red',
  },

  input: {
    backgroundColor: COLORS.whiteColor,
    color: COLORS.blackColor,
    borderRadius: '10@msr',
    height: '35@vs',
    position: 'relative',
    marginLeft: '10@s',
    fontSize: 11,
    paddingLeft: '10@s',
    paddingRight: '35@s',
    elevation: 3,
    shadowOpacity: 0.7,
  },
  icon: {
    position: 'absolute',
    right: '10@s',
    backgroundColor: COLORS.whiteColor,
  },
  details: {
    borderBottomLeftRadius: '20@msr',
    borderBottomRightRadius: '20@msr',
    borderTopLeftRadius: '20@msr',
    borderTopRightRadius: '20@msr',

    alignSelf: 'center',
    marginTop: '20@vs',
    borderRadius: '3@msr',
    width: '312@s',
    backgroundColor: COLORS.whiteColor,
    shadowColor: COLORS.blackColor,
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: '10@msr',
    elevation: 5,
    bottom: '20@vs',
  },
  imageview: {
    height: '200@vs',
    width: '312@s',

    borderTopLeftRadius: '20@msr',
    borderTopRightRadius: '20@msr',
  },
  favListView: {
    height: '34@vs',
    width: '34@s',
    borderWidth: '1@s',
    borderRadius: '10@msr',
    justifyContent: 'center',
    borderColor: '#c9c8ce',
  },
  titleTxt: {
    flex: 0.6,
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
    color: '#000',
  },
  thirdRowView: {
    flexDirection: 'row',
    flexDirection: 'row',
    paddingLeft: '15@s',
    paddingRight: '10@s',
    paddingBottom: '10@vs',
  },
  locationView: {
    flexDirection: 'row',
    margin: '5@msr',
    flexDirection: 'row',
    paddingLeft: '10@s',
  },
  titleView: {
    flexDirection: 'row',
    marginTop: '10@vs',
    paddingLeft: '20@s',
    flexDirection: 'row',
  },
  primaryButtonView: {
    paddingVertical: '15@vs',
    width: '250@s',
    backgroundColor: '#F15F5F',
    borderRadius: '10@msr',
    justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 40,
  },
});

export default Favorlist;
