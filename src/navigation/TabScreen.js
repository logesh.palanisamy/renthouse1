import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { COLORS, FONTS, Fonts, SIZES, Vector, Images } from '../constants';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScaledSheet } from 'react-native-size-matters';
import CommonHeader from '../components/CommonHeader';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Privacy from '../screens/privacy/Privacy';
import Favorlist from '../screens/favorList/Favorlist';
import TermsAndConditions from '../screens/termsAndConditions/TermsAndConditions';
import Home from '../screens/dashboard/Home';
import Hotels from '../screens/hotels/Hotels';
import Description from '../screens/description/Description';
import CustomDrawer from './CustomDrawer';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import IIcon from 'react-native-vector-icons/Ionicons';
import FIcon from 'react-native-vector-icons/FontAwesome5';

const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const HomeStack = createNativeStackNavigator();
const HotelStack = createNativeStackNavigator();
const FavStack = createNativeStackNavigator();
const MainStack = createNativeStackNavigator();

const TabScreen = ({ }) => {
  return (
    <Tab.Navigator
      initialRouteName="Homescreen"
      screenOptions={{
        tabBarStyle: styles.TabStyleBar,
        tabBarLabelStyle: styles.TabLabelStyle,
      }}>
      <Tab.Screen
        name={'Homescreen'}
        component={Homescreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Home',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({ color }) => (
            <FIcon name="home" color={color} size={26} />
          ),
        }}
      />

      <Tab.Screen
        name={'HotelStackScreen'}
        component={HotelStackScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Explore',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({ color }) => (
            <MIcon name="explore" color={color} size={26} />
          ),
        }}
      />

      <Tab.Screen
        name={'FavStackScreen'}
        component={FavStackScreen}
        options={{
          headerShown: false,
          tabBarLabel: 'Favourites',
          tabBarActiveTintColor: COLORS.primaryColor,
          tabBarIcon: ({ color }) => (
            <IIcon name="heart-circle-sharp" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const DrawerScreen = ({ }) => {
  return (
    <Drawer.Navigator
      drawerContent={props => <CustomDrawer {...props} />}
      initialRouteName="DrawerScreen">
      <Drawer.Screen
        name="DrawerScreen"
        component={MainScreens}
        options={{ headerShown: false }}
      />
    </Drawer.Navigator>
  );
};

const Homescreen = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name={'Home'}
        component={Home}
        options={{
          headerShown: false,
        }}
      />
    </HomeStack.Navigator>
  );
};

const HotelStackScreen = () => {
  return (
    <HotelStack.Navigator>
      <HotelStack.Screen
        name={'Hotels'}
        component={Hotels}
        options={{
          headerShown: false,
        }}
      />
    </HotelStack.Navigator>
  );
};

const FavStackScreen = () => {
  return (
    <FavStack.Navigator>
      <FavStack.Screen
        name={'Favorlist'}
        component={Favorlist}
        options={{
          headerShown: false,
        }}
      />
    </FavStack.Navigator>
  );
};

const MainScreens = ({ navigation }) => {
  return (
    <MainStack.Navigator>
      <MainStack.Screen
        name={'TabScreen'}
        component={TabScreen}
        options={{
          headerShown: false,
        }}
      />

      <MainStack.Screen
        name={'Privacy'}
        component={Privacy}
        options={{
          headerShown: false,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Hello'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'TermsAndConditions'}
        component={TermsAndConditions}
        options={{
          headerShown: false,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Hello'}
            />
          ),
        }}
      />

      <MainStack.Screen
        name={'Description'}
        component={Description}
        options={{
          headerShown: false,
          header: () => (
            <CommonHeader
              type={2}
              onClick={() => navigation.goBack()}
              title={'Hello'}
            />
          ),
        }}
      />
    </MainStack.Navigator>
  );
};

const styles = ScaledSheet.create({
  TabLabelStyle: { fontSize: '11@s', fontFamily: Fonts.Regular },
  TabStyleBar: {
    position: 'absolute',
    backgroundColor: '#0c0c25',
    height: SIZES.height / 13,
  },
  TabFocusBar: {
    height: '4.5@vs',
    width: '50@msr',
    backgroundColor: COLORS.primaryColor,
    borderBottomLeftRadius: '20@msr',
    borderBottomRightRadius: '20@msr',
    top: '-4@vs',
  },
  TabParentView: {
    alignItems: 'center',
  },
  TabMarginTop: { marginTop: '5@vs' },
  image: {
    width: '20@s',
    height: '20@vs',
  },
});
export { TabScreen, DrawerScreen };
