import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import {ScaledSheet} from 'react-native-size-matters';
import {isIphoneX} from '../utils';
import {COLORS, Images, FONTS, adjust, Vector} from '../constants';
import {CommonPopup} from '../components/CommonPopup';
import {AuthContext} from '../navigation/AuthProvider';

const FlatListData = [
  {
    id: 1,
    title: 'Home',
  },
  {
    id: 2,
    title: 'Privacy Policy',
  },
  {
    id: 3,
    title: 'Terms and Conditions',
  },
  {
    id: 4,
    title: 'Log out',
  },
];

const CustomDrawer = props => {
  const [logoutAction, setLogoutAction] = useState(false);
  const clickAction = item => {
    props.navigation.closeDrawer();
    if (item.id == 1) {
      props.navigation.navigate('Homescreen');
    } else if (item.id == 2) {
      props.navigation.navigate('Privacy');
    } else if (item.id == 3) {
      props.navigation.navigate('TermsAndConditions');
    } else if (item.id == 4) {
      setLogoutAction(true);
    }
  };

  const {user, logout} = useContext(AuthContext);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#0c0c25'}}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.view}>
          <TouchableOpacity onPress={() => props.navigation.closeDrawer()}>
            {Vector.BackArrow}
          </TouchableOpacity>
          {/* <Text style={styles.title}>Profile</Text> */}
        </View>
        <View style={styles.profileView}>
          <View style={styles.imgView}>
            <Image
              style={styles.imgStyle}
              source={require('../../assets/images/rentlogo.jpg')}
              resizeMode={'cover'}
            />
           
            <View
              style={{marginHorizontal: 15, justifyContent: 'center'}}></View>
          </View>
          <View>
            {FlatListData.map((item, index) => {
              return (
                <TouchableOpacity
                  key={item.id}
                  activeOpacity={0.3}
                  style={styles.flatListParentView}
                  onPress={() => clickAction(item)}>
                  <View style={{flex: 0.8, justifyContent: 'center'}}>
                    <Text numberOfLines={1} style={styles.tabTitle}>
                      {item.title}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            })}
          </View>
        </View>
      </ScrollView>
      <CommonPopup
        type={0}
        title={'Logout'}
        subtitle={'Are you sure want to logout?'}
        isVisible={logoutAction}
        successButtonText={'Logout'}
        negativeButtonText={'Cancel'}
        onPressNo={() => setLogoutAction(false)}
        onPressYes={() => logout()}
      />
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
  view: {
    height: isIphoneX() ? '50@vs' : '46@vs',
    marginHorizontal: '10@s',
    alignItems: 'center',
    flexDirection: 'row',
  },
  title: {
    marginStart: '6@s',
    fontSize: adjust(20),
    marginLeft: '50@s',
    fontWeight: '600',
    color: COLORS.primaryColor,
  },
  imgStyle: {
    height: '80@vs',
    width: '80@s',
    alignSelf: 'center',
 marginLeft:'60@s',
    borderRadius: '20@msr',
  },
  profileView: {
    paddingHorizontal: '15@s',
  },
  imgView: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.primaryColor,
    paddingVertical: '20@vs',
    flexDirection: 'row',
  },
  nameTitle: {
    color: COLORS.whiteColor,
    alignSelf: 'center',
    marginTop: '10@vs',
    ...FONTS.body2,
    paddingLeft: '20@s',
  },
  flatListParentView: {
    flexDirection: 'row',
    paddingVertical: '15@vs',
    borderBottomWidth: 0.6,
    borderBottomColor: COLORS.whiteColor,
  },
  tabTitle: {
    ...FONTS.body2,
    fontSize: adjust(14),
    color: COLORS.whiteColor,
  },
});

export default CustomDrawer;
