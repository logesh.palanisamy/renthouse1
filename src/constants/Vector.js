/* eslint-disable react-native/no-inline-styles */
// Vector Icons
import React from 'react';
import {COLORS, SIZES} from './Theme';
import MenuIcon from 'react-native-vector-icons/Ionicons';
import NotificationIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import BackIcon from 'react-native-vector-icons/AntDesign';
import UpArrowIcon from 'react-native-vector-icons/AntDesign';
import BackArrowIcon from 'react-native-vector-icons/AntDesign';
import DownArrowIcon from 'react-native-vector-icons/AntDesign';
import HeaderBackArrowIcon from 'react-native-vector-icons/AntDesign';
import DrawerRightIcon from 'react-native-vector-icons/AntDesign';
import CancelIcon from 'react-native-vector-icons/Feather';
import EyeIcon from 'react-native-vector-icons/Ionicons';
import EyeOffIcon from 'react-native-vector-icons/Ionicons';
import heartOffIcon from 'react-native-vector-icons/AntDesign';
import heartIcon from 'react-native-vector-icons/AntDesign';

const MenuImage = (
  <MenuIcon name={'ios-menu-sharp'} color={COLORS.whiteColor} size={25} />
);

const NotificationImage = (
  <NotificationIcon name={'bell'} color={COLORS.whiteColor} size={22} />
);

const Back = (
  <BackIcon name={'left'} color={COLORS.darkGrey} size={SIZES.icon} />
);

const BackArrow = (
  <BackArrowIcon name={'arrowleft'} color={COLORS.primaryColor} size={25} />
);

const HeaderBackArrow = (
  <HeaderBackArrowIcon name={'arrowleft'} color={COLORS.whiteColor} size={25} />
);

const DrawerRight = (
  <DrawerRightIcon name={'right'} color={COLORS.darkGrey} size={20} />
);

const CancelX = <CancelIcon name={'x'} color={COLORS.darkGrey} size={20} />;

const Eye = <EyeIcon name={'eye'} color={COLORS.darkGrey} size={25} />;

const EyeOff = (
  <EyeOffIcon name={'eye-off'} color={COLORS.darkGrey} size={25} />
);

const heart = (
  <heartIcon name={'heart'} color={COLORS.primaryColor} size={20} />
);

const heartOff = (
  <heartOffIcon name={'hearto'} color={COLORS.primaryColor} size={20} />
);

const DownArrow = (
  <DownArrowIcon name={'down'} color={COLORS.darkGrey} size={23} />
);

const UpArrow = <UpArrowIcon name={'up'} color={COLORS.darkGrey} size={23} />;

export default {
  Back,
  MenuImage,
  NotificationImage,
  BackArrow,
  DrawerRight,
  HeaderBackArrow,
  CancelX,
  DownArrow,
  UpArrow,
  Eye,
  EyeOff,
  heart,
  heartOff,
};
