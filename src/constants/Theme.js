//--- Font Family , Color codes
import {PixelRatio, Dimensions} from 'react-native';
const {width, height} = Dimensions.get ('window');
const pixelRatio = PixelRatio.get (); //Ratio for getting density of mobile devices

const deviceHeight = Dimensions.get ('window').height;
const deviceWidth = Dimensions.get ('window').width;
//Responsive Layout Calculations
export const adjust = size => {
  if (pixelRatio >= 2 && pixelRatio < 3) {
    // iphone 5s and older Androids
    if (deviceWidth < 360) {
      return size * 0.95;
    }
    // iphone 5
    if (deviceHeight < 667) {
      return size;
      // iphone 6-6s
    }
    if (deviceHeight >= 667 && deviceHeight <= 735) {
      return size * 1.15;
    }
    // older phablets
    return size * 1.25;
  }
  if (pixelRatio >= 3 && pixelRatio < 3.5) {
    // catch Android font scaling on small machines
    // where pixel ratio / font scale ratio => 3:3
    if (deviceWidth <= 360) {
      return size;
    }
    // Catch other weird android width sizings
    if (deviceHeight < 667) {
      return size * 1.15;
      // catch in-between size Androids and scale font up
      // a tad but not too much
    }
    if (deviceHeight >= 667 && deviceHeight <= 735) {
      return size * 1.2;
    }
    // catch larger devices
    // ie iphone 6s plus / 7 plus / mi note 等等
    return size * 1.27;
  }
  if (pixelRatio >= 3.5) {
    // catch Android font scaling on small machines
    // where pixel ratio / font scale ratio => 3:3
    if (deviceWidth <= 360) {
      return size;
      // Catch other smaller android height sizings
    }
    if (deviceHeight < 667) {
      return size * 1.2;
      // catch in-between size Androids and scale font up
      // a tad but not too much
    }
    if (deviceHeight >= 667 && deviceHeight <= 735) {
      return size * 1.25;
    }
    // catch larger phablet devices
    return size * 1.4;
  }
  return size;
};

const Palette = {
  white: '#ffffff',
  black: '#000000',
  primaryColor: '#F15F5F',
  secondaryColor: '#0C0C25',
  tertiaryColor:'3D7AA7',
  lightGrey: '#C4C4C4',
  darkGrey: '#696969',
  backgroundColour: '#F9F9F9',
};

export const COLORS = {
  whiteColor: Palette.white,
  blackColor: Palette.black,
  primaryColor: Palette.primaryColor,
  lightGrey: Palette.lightGrey,
  darkGrey: Palette.darkGrey,
  secondaryColor: Palette.secondaryColor,
  backgroundColour: Palette.backgroundColour,
};

export const SIZES = {
  // global sizes
  base: 8,
  font: 14,
  radius: 40,
  padding: 10,
  padding2: 12,

  //Vector Icons size
  icon: 20,
  secondIcon: 15,

  icon_Color: '#273B4A',

  // font sizes
  largeTitle: 19,
  h0: 10,
  t0: 13,
  P0: 12,
  f0: 9,
  P1: 18,
  c0: 15,

  h1: 28,
  h2: 20,
  h3: 18,
  h4: 16,
  h5: 13,
  body1: 28,
  body2: 18,
  body3: 14,
  body4: 12,
  body5: 10,
  body6: 7,

  t1: 9,
  t2: 12,
  e1: 6,
  // app dimensions
  width,
  height,
};
export const Fonts = {
  Bold: 'Poppins-Bold',
  Regular: 'Poppins-Regular',
  Medium: 'Poppins-Medium',
  SemiBold: 'Poppins-SemiBold',
  italic: 'Poppins-Italic',
  MediumItalic: 'Poppins-MediumItalic',
};

export const FONTS = {
  largeTitle: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.largeTitle),
  },
  h0: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.h0),
  },
  t0: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.t0),
  },
  P0: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.P0),
  },
  f0: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.f0),
  },
  P1: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.P1),
  },
  c0: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.c0),
  },
  i: {
    fontFamily: Fonts.italic,
    fontSize: adjust (SIZES.body5),
  },
  i0: {
    fontFamily: Fonts.MediumItalic,
    fontSize: adjust (SIZES.body5),
  },

  h1: {
    fontFamily: Fonts.Bold,
    fontSize: adjust (SIZES.h1),
  },
  h2: {
    fontFamily: Fonts.Regular,
    fontSize: adjust (SIZES.h2),
  },
  h3: {
    fontFamily: Fonts.Bold,
    fontSize: adjust (SIZES.h3),
  },
  h4: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.h4),
  },
  h5: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.h5),
  },
  h6: {
    fontFamily: Fonts.Bold,
    fontSize: adjust (SIZES.body5),
  },

  body1: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.body1),
  },
  body2: {
    fontFamily: Fonts.Medium,
    fontSize: adjust (SIZES.body2),
  },
  body3: {
    fontFamily: Fonts.Regular,
    fontSize: adjust (SIZES.body3),
  },
  body4: {
    fontFamily: Fonts.Regular,
    fontSize: adjust (SIZES.body4),
  },
  body5: {
    fontFamily: Fonts.Regular,
    fontSize: adjust (SIZES.body5),
  },
  body6: {
    fontFamily: Fonts.Regular,
    fontSize: adjust (SIZES.body6),
  },
};
const appTheme = {COLORS, SIZES, FONTS, Fonts, adjust};

export default appTheme;
