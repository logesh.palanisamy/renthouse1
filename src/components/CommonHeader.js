import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import {COLORS, FONTS, adjust, Vector, Images} from '../constants';
import {isIphoneX} from '../utils';
import {ScaledSheet} from 'react-native-size-matters';
const CommonHeader = ({
  onClick,
  type,
  title,
  notifyNavigation,
  cartNavigation,
}) => {
  return <SafeAreaView style={Styles.container}></SafeAreaView>;
};
const Styles = ScaledSheet.create({
  container: {
    backgroundColor: COLORS.primaryColor,
    height: isIphoneX() ? '50@vs' : '46@vs',
  },

  firstFlex: {
    flex: 0.5,
    alignItems: 'center',
    flexDirection: 'row',
  },
  secondFlex: {
    flex: 0.5,
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  menu: {marginLeft: '15@vs', alignItems: 'center'},
  notify: {marginRight: '15@vs', alignItems: 'center'},
  imageStyle: {
    height: '30@vs',
    width: '48@s',
    resizeMode: 'contain',
    marginLeft: '10@vs',
  },
  headerTitle: {
    ...FONTS.t0,
    fontSize: adjust(12),
    color: COLORS.whiteColor,
    marginLeft: '10@s',
  },
});
export default CommonHeader;
