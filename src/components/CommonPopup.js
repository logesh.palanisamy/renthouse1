import {StyleSheet, Text, View, Alert, Dimensions} from 'react-native';
import React, {useState} from 'react';
import Modal from 'react-native-modal';
import {ScaledSheet} from 'react-native-size-matters';
import {TextInputField} from '../../components/TextInputField';
import {TextInput} from 'react-native-paper';
import {
  deviceHeight,
  deviceWidth,
  FONTS,
  palette,
  SIZES,
  COLORS,
  adjust,
} from '../constants/Theme';
import CommonButton from '../components/CommonButton';
var {height, width} = Dimensions.get('window');
export const CommonPopup = ({
  isVisible,
  title,
  subtitle,
  onPressNo,
  onPressYes,
  successButtonText,
  negativeButtonText,
  type,
}) => {
  return (
    <Modal
      animationIn={'slideInUp'}
      isVisible={isVisible}
      style={styles.container}>
      {type === 0 ? (
        <View style={styles.containerStyle}>
          <Text style={styles.order}>{title}</Text>
          <Text style={styles.order_2}>{subtitle}</Text>
          <View style={styles.buttonContainer}>
            <CommonButton
              buttonStyle={styles.ButtonStyle_No}
              isLoading={false}
              text={negativeButtonText}
              textStyle={styles.buttonTextStyle_No}
              onPress={onPressNo}
            />
            <CommonButton
              buttonStyle={styles.ButtonStyle_yes}
              isLoading={false}
              text={successButtonText}
              textStyle={styles.buttonTextStyle_yes}
              onPress={onPressYes}
            />
          </View>
        </View>
      ) : type === 1 ? (
        <View style={styles.containerStyle_1}>
          <Text style={styles.order}>{title}</Text>
          <Text style={styles.order_2}>{subtitle}</Text>
          <TextInput
            style={styles.input2}
            mode={'outlined'}
            placeholderTextColor={'#B0B0B0'}
            activeOutlineColor={'#8B84D7'}
            outlineColor={'#8B84D7'}
            multiline={true}
          />
          <View style={styles.buttonContainer}>
            <CommonButton
              buttonStyle={styles.ButtonStyle_No}
              isLoading={false}
              text={'Cancel'}
              textStyle={styles.buttonTextStyle_No}
              onPress={() => Alert.alert('Octo', 'Cancelled')}
            />
            <CommonButton
              buttonStyle={styles.ButtonStyle_yes}
              isLoading={false}
              text={'Submit'}
              textStyle={styles.buttonTextStyle_yes}
              onPress={() => Alert.alert('Octo', 'Submitted')}
            />
          </View>
        </View>
      ) : null}
    </Modal>
  );
};
const styles = ScaledSheet.create({
  containerStyle: {
    width: width - 30,

    backgroundColor: 'white',
    borderRadius: '4@msr',
    justifyContent: 'center',
    alignSelf: 'center',
    paddingVertical: '12@msr',
  },
  containerStyle_1: {
    width: width - 30,

    backgroundColor: 'white',
    borderRadius: '4@msr',
    justifyContent: 'center',
    alignSelf: 'center',

    paddingVertical: '12@msr',
  },
  container: {
    height: '600@vs',
  },
  order: {
    ...FONTS.h3,

    paddingHorizontal: '10@msr',
    paddingVertical: '5@vs',
    color: COLORS.blackColor,
  },
  order_2: {
    ...FONTS.body4,
    paddingHorizontal: '10@msr',
    paddingVertical: '5@vs',
    color: COLORS.blackColor,
  },
  buttonContainer: {
    flexDirection: 'row',

    marginVertical: '5@vs',
    justifyContent: 'space-between',
  },
  //buttons
  //no
  buttonTextStyle_No: {
    ...FONTS.P0,
    fontSize: adjust(12),
    fontWeight: '800',
    color: COLORS.primaryColor,
  },
  ButtonStyle_No: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust(5),
    backgroundColor: COLORS.whiteColor,
  },
  //no
  //yes
  buttonTextStyle_yes: {
    ...FONTS.P0,
    fontSize: adjust(12),
    fontWeight: '800',
    color: COLORS.whiteColor,
  },
  ButtonStyle_yes: {
    height: '40@vs',
    width: '135@s',
    borderRadius: adjust(5),
    backgroundColor: COLORS.primaryColor,
  },
  //yes
  //Buttons
  //Modal 2
  input2: {
    height: '100@vs',
    width: '305@s',
    color: COLORS.blackColor,
    backgroundColor: '#FBFBFB',
    borderColor: '#FFFFFF',
    borderWidth: 0.5,
    borderRadius: '5@msr',
    fontSize: adjust(16),
    textAlign: 'left',
    marginTop: '10@msr',
    alignSelf: 'center',
  },
});
